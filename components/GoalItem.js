import React from 'react';
import { Text, TouchableNativeFeedback, View, StyleSheet } from 'react-native';

const GoalItem = (props) => {
    const { title, onDelete, id } = props
    return (
        <TouchableNativeFeedback onPress={() => onDelete(id)}>
            <View style={styles.listItem} >
                <Text >{title}</Text>
            </View>
        </TouchableNativeFeedback>
    );
}

const styles = StyleSheet.create({
    listItem: {
        padding: 10,
        marginVertical: 10,
        backgroundColor: "#ccc",
        borderColor: "black",
        borderWidth: 1,
    }
});

export default GoalItem;