import React, { useState } from 'react';
import { Button, TextInput, View, StyleSheet, Modal } from 'react-native';

const GoalInput = ({ onAddGoal, visible, onCancel }) => {
    const [enteredGoal, setEnteredGoal] = useState('');

    const goalInputHandler = (enteredText) => {
        setEnteredGoal(enteredText);
    }

    const addGoalHandler = () => {
        onAddGoal(enteredGoal);
        setEnteredGoal('');
    }

    return (
        <Modal visible={visible} animationType="slide" >
            <View style={styles.inputContainer} >
                <TextInput
                    style={styles.input}
                    placeholder="Text me..."
                    onChangeText={goalInputHandler}
                    value={enteredGoal}
                />
                <View style={styles.buttonContainer} >
                    <View style={styles.button}>
                        <Button
                            title="CANCEL"
                            color="red"
                            onPress={() => onCancel()}
                        />
                    </View>
                    <View style={styles.button}>
                        <Button
                            title="ADD"
                            onPress={addGoalHandler}
                        />
                    </View>
                </View>
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    button: {
        width: "40%",
    },
    buttonContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "50%",
    },
    input: {
        width: "80%",
        borderColor: "black",
        borderWidth: 1,
        padding: 10,
        marginBottom: 10
    },
    inputContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default GoalInput;